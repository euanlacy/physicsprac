import math


class Vector:

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return str([self.x, self.y, self.z])

    def __abs__(self):
        return math.sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)

    def __mul__(self, other):
        return Vector(self.x * other, self.y * other, self.z * other)

    def __rmul__(self, other):
        return self * other

    def __truediv__(self, other):
        return self * (1 / other)

    def __rtruediv__(self, other):
        return self / other

    def unit_vec(self):
        mag = abs(self)
        return self / mag

    def cross(self, vector2):
        x = self.y * vector2.z - self.z * vector2.y
        y = vector2.x * self.z - self.x * vector2.z
        z = self.x * vector2.y - self.y * vector2.x
        new_vec = Vector(x, y, z)

        return new_vec
