from math import *
import matplotlib.pyplot as plt
from vector import Vector

locations = {'Scotland': 5.99e-4, 'Bolivia': 3.17e-4, 'Singapore': 5.50e-4}
swing_speeds = {'105': 46.95, '115': 51.4, '125': 55.85}


def num_model(v_initial, angle, delta_t, spin, k_drag):
    g = -9.81
    radius = 0.02055  # m
    area = pi * radius ** 2
    theta = radians(angle)
    air_density = 1.225
    mass = 0.04593  # kg

    v_initial *= 1.48

    v = Vector(v_initial * cos(theta), v_initial * sin(theta), 0)

    vel = [v]

    # r = ((v_initial ** 2) * sin(2 * theta)) / -g

    # print("Range: {:.2f}m".format(r))

    coords = [(0, 0)]

    def coeff_lift(velocity, spin):
        return -0.05 + sqrt(0.0025 + 0.39 * radius * (abs(spin) / abs(velocity)))

    def new_vel(old, a):
        return old + a * delta_t

    def new_pos(old, v):
        return old + v * delta_t

    def lift_acc(velocity, spin):
        spin_vec = Vector(0, 0, -1)
        lift_vec = velocity.cross(spin_vec).unit_vec()
        k = coeff_lift(velocity, spin)
        force = 0.5 * k * air_density * area * (abs(velocity) ** 2) * lift_vec

        return force / mass

    def drag_acc(velocity):
        v_dir = -1 * velocity.unit_vec()
        force = k_drag * (abs(velocity) ** 2) * v_dir
        return force / mass

    while True:
        curr_v = vel[-1]
        curr_pos = coords[-1]

        acc_drag = drag_acc(curr_v)
        acc_lift = lift_acc(curr_v, spin)

        # print(f"Drag Acceleration: {acc_drag}")
        # print(f"Lift Acceleration: {acc_lift}")

        acc_x = acc_drag.x + acc_lift.x
        acc_y = acc_drag.y + acc_lift.y + g

        # print(f"Acceleration\nx: {acc_x:.2f}\ny: {acc_y:.2f}")

        new_v_x = new_vel(curr_v.x, acc_x)
        new_v_y = new_vel(curr_v.y, acc_y)

        new_v = Vector(new_v_x, new_v_y, 0)

        vel.append(new_v)
        coords.append((new_pos(curr_pos[0], new_v_x), new_pos(curr_pos[1], new_v_y)))

        # print(f"x: {new_v.x}\ny: {new_v.y}\n\n")
        if coords[-1][1] <= 0:
            break

    return coords


def bestAngle(swing_speed, time_step, spin, location):
    angles = list(range(5, 60, 1))
    ranges = []

    for theta in angles:
        coords = num_model(swing_speeds[swing_speed], theta, time_step, spin, locations[location])
        distance = coords[-1][0]
        ranges.append(distance)

    max_dist = max(ranges)

    best_angle = angles[ranges.index(max_dist)]

    print(f"The optimum angle for {location}, with a swing speed of {swing_speed}mph, is {best_angle}° with a range of {max_dist:.2f}m")

    plt.scatter(angles, ranges, s=1)

    plt.minorticks_on()

    plt.grid(b=True, which='major', linestyle='-')
    plt.grid(b=True, which='minor', linestyle='-', alpha=0.2)

    plt.xlabel("Angle [degrees]")
    plt.ylabel("Horizontal Distance [m]")

    plt.title(f"Optimum angle in {location} for a swing speed of {swing_speed}mph")
    plt.show()


def trajectory(v_initial, angle, time_step, spin, location):
    coords = num_model(swing_speeds['125'], angle, time_step, spin, locations[location])

    print(f"Range {(coords[-1][0]):.2f}")

    plt.scatter(*zip(*coords), s=0.1)

    plt.xlabel("Horizontal Distance [m]")
    plt.ylabel("Vertical Distance [m]")

    plt.minorticks_on()

    plt.grid(b=True, which='major', linestyle='-')
    plt.grid(b=True, which='minor', linestyle='-', alpha=0.2)

    plt.ylim(0, 80)
    plt.xlim(0, 150)

    plt.title(f"Flight path of a golf ball in {location} with a swing speed of {v_initial}mph")

    plt.show()


angle = 80  # From x-axis anti-clockwise degrees
time_step = 0.001  # Time in between each calculation seconds
spin = 260  # rad/s

for loc, k in locations.items():
    print(f"At {loc}")
    for mph, ms in swing_speeds.items():
        bestAngle(mph, time_step, spin, loc)

trajectory("125", 30, time_step, spin, "Scotland")
